<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\UserRole;

Route::group(['middleware' => 'web'], function () {

    $role[UserRole::ADMIN]      = UserRole::ADMIN;
    $role[UserRole::CITY_ADMIN] = implode('+', [UserRole::ADMIN, UserRole::CITY_ADMIN]);
    $role[UserRole::LC]         = implode('+', [$role['city_admin'], UserRole::LC]);
    $role[UserRole::REP]        = implode('+', [$role['city_admin'], UserRole::REP]);
    $role[UserRole::DRIVER]     = implode('+', [$role['city_admin'], UserRole::DRIVER]);

    Route::group(['namespace' => UserRole::ADMIN, 'middleware' => ['auth', 'authorize:'.$role[UserRole::ADMIN]]], function () {
//        $this->get('country', 'CountryController@index')->name('country');
//        $this->get('city', 'CityController@index')->name('city');
//        $this->get('city_admin', 'UserController@city_admin')->name('city_admin');
        $this->resources([
            'country'           => 'CountriesController',
            'city'              => 'CitiesController',
            'passenger-type'    => 'PassengerTypesController',
        ]);
    });



    Route::group(['namespace' => UserRole::CITY_ADMIN, 'middleware' => ['auth', 'authorize:'.$role[UserRole::CITY_ADMIN]]], function () {
        $this->get('upload-data', 'UploadController@upload_data')->name('upload-data');
        $this->post('arrival-list', 'UploadController@arrival_list')->name('arrival-list');
        $this->post('transfer-list', 'UploadController@transfer_list')->name('transfer-list');

        $this->resources([
            'user'   =>'UsersController',
            'hotel'  =>'HotelsController'
        ]);

//        $this->get('lc', 'UserController@lc')->name('lc');
//        $this->get('rep', 'UserController@lc')->name('rep');
//        $this->get('driver', 'UserController@lc')->name('driver');
//
//        $this->get('hotel', 'HotelController@index')->name('hotel');
//
//        $this->get('passenger-type', 'PassengerTypeController@index')->name('passenger-type');
//
//        $this->get('telegram', 'TelegramController@index')->name('telegram');
    });

    Route::group(['prefix' => '/', 'middleware'=>['auth']], function() {

        $this->get('dashboard', function () {
            return view('dynamic-master', ['page'=>'dashboard']);
        })->name('dashboard');

        $this->resources([
            'booking'   => 'BookingsController'
        ]);

        $this->get('logout', 'Auth\LoginController@logout')->name('logout');

    });

});
// Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
//     $this->get('upload-data', 'AdminController@upload_data')->name('upload-data');
//     $this->post('arrival-list', 'UploadController@arrival_list')->name('arrival-list');
//     $this->post('transfer-list', 'UploadController@transfer_list')->name('transfer-list');
//     $this->get('lc', 'AdminController@lc')->name('admin.lc');
// });


Route::match(['get', 'post'], '/botman', 'BotManController@handle');
Route::get('/botman/tinker', 'BotManController@tinker');

Auth::routes();

Route::get('/', function (){
    return redirect(route('login'));
});
