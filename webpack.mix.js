let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/assets/js/app.js', 'public/js')
//    .sass('resources/assets/sass/app.scss', 'public/css');

// admin :
mix.sass('resources/assets/sass/admin.scss', 'public/css');
// mix.js('resources/assets/js/admin-panel.js', 'public/js');
mix.combine([
    'resources/assets/js/home/jquery.js',
    'resources/assets/js/files/bootstrap.min.js',
    'resources/assets/js/files/select2.min.js',
    'resources/assets/js/files/sweetalert.min.js',
    'resources/assets/js/files/scripts.js',
], 'public/js/admin.js');

// home :
mix.sass('resources/assets/sass/home.scss', 'public/css');
mix.combine([
    'resources/assets/js/home/jquery.js',
    'resources/assets/js/files/bootstrap.min.js',
    'resources/assets/js/home/jq_easy.js',
    'resources/assets/js/home/scrollreveal.js',
    'resources/assets/js/home/jquery.magnific-popup.js',
    'resources/assets/js/home/creative.js',
    'resources/assets/js/files/select2.min.js',
    'resources/assets/js/files/sweetalert.min.js'
], 'public/js/home.js');

mix.combine(
    ['resources/assets/js/files/scripts.js'],
    'public/js/scripts.js'
);

// login :
mix.sass('resources/assets/sass/login.scss', 'public/css').version();
mix.combine([
    'resources/assets/js/home/jquery.js',
    'resources/assets/js/files/bootstrap.min.js',
    'resources/assets/js/files/sweetalert.min.js',
], 'public/js/login.js');
