@section('content')

    <div class="row">
        <form action="{{ route('user.store') }}" method="post">
            @csrf
            @include('section.text_field', ['key' => 'firstname', 'id' => 'firstname', 'persian_key' => 'نام',
            'value' => '', 'optional'=>true])

            @include('section.text_field', ['key' => 'lastname', 'id' => 'lastname', 'persian_key' => 'نام خانوادگی',
            'value' => '', 'optional'=>true])

            @include('section.text_field', ['key' => 'username', 'id' => 'username', 'persian_key' => 'نام کاربری',
            'value' => ''])

            @include('section.text_field', ['key' => 'password', 'id' => 'password', 'persian_key' => 'رمز عبور',
            'value' => ''])

            @include('section.text_field', ['key' => 'phonenumber', 'id' => 'phonenumber', 'persian_key' => 'شماره تلفن',
            'value' => '', 'optional'=>true])

            <?php
            $role_objects = [];
            $roles_array = [\App\UserRole::LC, \App\UserRole::REP, \App\UserRole::DRIVER];
            foreach($roles_array as $role)
            {
                $role_object        = new StdClass;
                $role_object->id    = $role;
                $role_object->role  = $role;
                $role_objects[] = $role_object;
            }
            ?>
            @include('section.select_field',
                ['key' => 'role', 'id' => 'role', 'persian_key' => 'نقش',
                'objects' => $role_objects, 'opt_prop' => 'role', 'current_id' => \App\UserRole::LC])
            
            @include('section.submit_button', ['value'=>'ثبت', 'btn_cls'=>'btn-info'])
        </form>

    </div>
    <div class="row ltr">
        <div class="table-responsive">
            <table class="table table-hover table-bordered table-hover" style="border: 2px solid white">
                <thead>
                <tr>
                    <th>نام</th>
                    <th>نام خانوادگی</th>
                    <th>نقش</th>
                    <th>شماره تلفن</th>
                    <th>نام کاربری</th>
                    <th>تنظیمات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                    <?php $phonenumber = $user->phonenumber()->first() ?>
                        <td>{{ $user->firstname }}</td>
                        <td>{{ $user->lastname }}</td>
                        <td>{{ $user->role }}</td>
                        <td>{{ $phonenumber?$phonenumber->phonenumber:'' }}</td>
                        <td>{{ $user->username }}</td>
                        <td>
                            @include('section.deledit_form', [
                                'delroute' => route('user.destroy',['id'=>$user->id]),
                                'editroute' => route('user.edit',['id'=>$user->id])
                                ])
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>


@stop