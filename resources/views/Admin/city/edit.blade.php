@section('content')

    <div class="row">
        <form action="{{ route('city.update',['id'=>$city->id]) }}" class="ltr" method="post">
            @csrf
            {{ method_field('PATCH') }}

            @include('section.text_field', ['key' => 'title', 'id' => 'title', 'persian_key' => 'نام',
            'value' => $city->title])

            @include('section.select_field',
                ['key' => 'timezone_id', 'id' => 'timezone_id', 'persian_key' => 'منطقه زمانی',
                'objects' => $timezones, 'opt_prop' => 'text', 'current_id' => $city->timezone_id])
            
            @include('section.select_field',
                ['key' => 'country_id', 'id' => 'country_id', 'persian_key' => 'کشور',
                'objects' => $countries, 'opt_prop' => 'title', 'current_id' => $city->country_id])

            @include('section.submit_button', ['value'=>'ویرایش', 'btn_cls'=>'btn-info'])
        </form>
    </div>
@stop