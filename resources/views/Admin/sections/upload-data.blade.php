<label class="col col-sm-5" for="arrival_list"><span class="glyphicon glyphicon-upload"></span> آپلود لیست ورودی ها</label>
<form method="post" enctype="multipart/form-data" action="{{ route('arrival-list') }}">
    {{ csrf_field() }}
    <input type="hidden" name="city_id" value="{{ auth()->user()->role=='city_admin'?auth()->user()->city_id:(old('city_id')?old('city_id'):'') }}" />
    <input onchange="return this.name.length ? form.submit() : false" type="file" id="arrival_list" style="display: none;" name="arrival_list" />
</form>

<label class="col col-sm-5" for="transfer_list"><span class="glyphicon glyphicon-upload"></span> آپلود لیست تکمیلی</label>
<form method="post" enctype="multipart/form-data" action="{{ route('transfer-list') }}">
    {{ csrf_field() }}
    <input type="hidden" name="city_id" value="{{ auth()->user()->role=='city_admin'?auth()->user()->city_id:(old('city_id')?old('city_id'):'') }}" />
    <input onchange="return this.name.length ? form.submit() : false" type="file" id="transfer_list" style="display: none;" name="transfer_list" />
</form>
