@section('content')

    <div class="row">
        <form action="{{ route('passenger-type.store') }}" method="post">
            @csrf

            @include('section.text_field', ['key' => 'type', 'id' => 'type', 'persian_key' => 'نوع',
            'value' => ''])

            @include('section.submit_button', ['value'=>'ثبت', 'btn_cls'=>'btn-info'])
        </form>

    </div>
    <div class="row ltr">
        <div class="table-responsive">
            <table class="table table-hover table-bordered table-hover" style="border: 2px solid white">
                <thead>
                <tr>
                    <th>ردیف</th>
                    <th>نوع</th>
                    <th>تنظیمات</th>
                </tr>
                </thead>
                <tbody>
                <?php $index = 0; ?>
                @foreach($passengerTypes as $ptype)
                    <tr>
                        <td>{{ ++$index }}</td>
                        <td>{{ $ptype->type }}</td>
                        <td>
                            @include('section.deledit_form', [
                            'delroute' => route('passenger-type.destroy',['id'=>$ptype->id]),
                            'editroute' => route('passenger-type.edit',['id'=>$ptype->id])
                            ])
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>


@stop