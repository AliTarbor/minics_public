<form class="btn-group-sm" action="{{ $delroute }}"
                                  method="post">
    @csrf
    {{ method_field('delete') }}
    <a href="{{ $editroute }}" class="btn btn-primary">ویرایش</a>
    <button class="btn btn-danger">حذف</button>
</form>