<div class="col-sm-6">
    <div class="form-group">
        <label for="{{ $id }}">{{ $persian_name }}</label>
        <input type="checkbox" id="{{ $id }}" class="form-control" name="{{ $name }}"{{ $checked?'checked':'' }}>
    </div>
</div>
