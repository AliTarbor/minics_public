@extends('Home.master')
@section('styles')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="{{ mix('/css/login.css') }}">
	<style>
		#mainNav {
			background-color: #34495e !important;
		}
	</style>
@endsection
@section('content')
	<div class="container">
		<form class="form-signin ajaxable" method="post" ajax-data-type="JSON" ajax-function="login_callback" action="{{ route('login') }}">
			<br>
			<h2 class="form-signin-heading text-center" style="font-weight: bold">ورود به سرویس</h2>
			{{ csrf_field() }}
			@if(count($errors)>0)
				<div class="alert alert-danger">
					<ul>
						@foreach($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
			<div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
				<label for="inputEmail" class="sr-only">نام کاربری</label>
				<input type="username" name="username" id="inputEmail" class="form-control text-center" placeholder="نام کاربری"
				       required autofocus>
				@if ($errors->has('username'))
					<span class="help-block">
					<strong>{{ $errors->first('username') }}</strong>
					</span>
				@endif
			</div>
			<div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
				<label for="inputPassword" class="sr-only">رمز عبور</label>
				<input name="password" type="password" id="inputPassword" class="form-control text-center"
				       placeholder="رمز عبور"
				       required>
				@if ($errors->has('password'))
					<span class="help-block">
					<strong>{{ $errors->first('password') }}</strong>
					</span>
				@endif
			</div>

			<div class="checkbox">
				<label>
					<input type="checkbox" name="remember" value="remember-me" {{ old('remember') ? 'checked' : '' }}> مرا به خاطر
					بسپار
				</label>
			</div>
			<button class="btn btn-lg btn-primary btn-block" type="submit">ورود</button>
		</form>
	</div> <!-- /container -->
@endsection
@section('scripts')
	<script src="{{ mix('/js/login.js') }}"></script>
	<script src="{{ mix('/js/scripts.js') }}"></script>
@endsection