@extends('booking_show')
@section('edit_booking')
<div class="row">
    <form action="{{ route('booking.update',['id'=>$booking->id]) }}" class="ltr" method="post">
        @csrf
        {{ method_field('PATCH') }}

        @include('section.check_field',
            ['name' => 'no_show', 'id' => 'no_show', 'persian_name' => 'گزارش عدم حضور مسافر',
            'checked'=>!empty($booking->no_show)])

            @include('section.submit_button', ['value'=>'ثبت', 'btn_cls'=>'btn-info'])
    </form>
</div>
@stop