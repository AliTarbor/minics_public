<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CityAbbreviation extends Model
{

    protected $fillable = ['city_id', 'abbr'];

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }
}
