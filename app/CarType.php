<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarType extends Model
{
    protected $fillable = [
        'car_transfer_type_id',
        'type'
    ];

    public function carTransferType()
    {
        return $this->belongsTo(CarTransferType::class,'car_transfer_type_id');
    }
}
