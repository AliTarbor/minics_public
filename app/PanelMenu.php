<?php

namespace App;

class PanelMenu
{
    const ALL = [UserRole::ADMIN => [
            'dashboard'         =>  'پنل اصلی',
            'country.index'     =>  'مدیریت کشورها',
            'city.index'        =>  'مدیریت شهرها',
            'upload-data'       =>  'آپلود اطلاعات رزرو',
            'user.index'        =>  'مدیریت کاربران',
            'passenger-type.index' =>  'مدیریت انواع مسافران',
            'booking.index'     =>  'مدیریت رزروها',
            'hotel.index'         =>  'مدیریت هتل ها',
            // 'telegram'      =>  'مدیریت پنل تلگرام',
        ],
        UserRole::CITY_ADMIN => [
            'dashboard'     =>  'پنل اصلی',
            'upload-data'   =>  'آپلود اطلاعات رزرو',
            'booking.index' =>  'مدیریت رزروها',
            'hotel.index'   =>  'مدیریت هتل ها',
            'user.index'    =>  'مدیریت کاربران',
            // 'telegram'      =>  'مدیریت پنل تلگرام',
        ],
        UserRole::REP => [
            'dashboard'     =>  'پنل اصلی',
            'booking.index' =>  'مشاهده رزرو ها',
        ],
        UserRole::DRIVER => [
            'dashboard'     =>  'پنل اصلی',
            'booking.index' =>  'مشاهده رزرو ها',
        ],
        UserRole::LC => [
            'dashboard'     =>  'پنل اصلی',
            'booking.index' =>  'مشاهده رزرو ها',
        ]
    ];
}
