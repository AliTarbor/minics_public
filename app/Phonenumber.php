<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phonenumber extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsToMany(User::class, 'user_phonenumbers');
    }


    public function passenger()
    {
        return $this->belongsToMany(Passenger::class, 'passenger_phonenumbers');
    }
}
