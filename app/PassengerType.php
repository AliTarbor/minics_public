<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PassengerType extends Model
{
    protected $fillable = [
        'type'
    ];
}
