<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomType extends Model
{
    use ManyHandler;
    protected $fillable = [
        'type'
    ];

    public function roomType()
    {
        return $this->belongsToMany(Booking::class, 'booking_roomtype');
    }
}
