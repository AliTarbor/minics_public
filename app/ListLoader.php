<?php

namespace App;

use Maatwebsite\Excel\Facades\Excel;

use App\Exceptions\UnRecognisedListException;
use App\Exceptions\ListNotLoadedException;
use App\Exceptions\EmptyListException;

abstract class ListLoader
{
    protected $city_id;

    protected $result_array         = [];
    protected $first_header_keys    = [];
    protected $second_header_keys   = [];
    protected $saved_cache          = [];

    protected $hotels               = [];
    protected $airlines             = [];
    protected $room_types           = [];
    protected $checkin_date_times   = [];
    protected $checkout_date_times  = [];
    protected $checkin_flight_no    = [];
    protected $checkout_flight_no   = [];
    protected $passengers           = [];

    public function load($path, $city_id)
    {
        $this->city_id = $city_id;
        \Config::set('excel.import.heading', 'numeric');
        $this->result_array = Excel::load($path, function ($reader) {
        })->get()->toArray();
        $this->validate();
        $this->instantiate();
    }

    protected function validate()
    {
        $this->validateLoaded();
        $this->validateRowCount();
        $this->validateColumnCount();
        $this->validateColumnNames($this->required_columns);
    }

    protected function validateColumnNames($colarray)
    {
        $counts = $this->count_values($this->result_array[1]);
        foreach($colarray as $col=>$times)
        {
            if(!isset($counts[$col]) || $times != $counts[$col])
                throw new UnRecognisedListException('Column "'.$col.'" not found!');
        }
    }

    protected function validateLoaded()
    {
        if(count($this->result_array) === 0)
            throw new ListNotLoadedException('List is not loaded or empty!');
    }

    protected function validateRowCount()
    {
        if(count($this->result_array) < 2)
            throw new EmptyListException('Uloaded file looks empty!');
    }

    protected function validateColumnCount()
    {
        if(count($this->result_array[0])<9)
            throw new UnRecognisedListException('Some columns missing!');
    }

    protected function instantiate()
    {
        $this->fillSecondHeaderKeys();
    }

    public abstract function storeToDB();

    protected function fillSecondHeaderKeys()
    {
        $counts = $this->count_values($this->result_array[1]);
        foreach($this->result_array[1] as $key=>$value)
        {
            if(!empty($value)) {
                $val = strtolower(trim($value));
                if(isset($counts[$val]) && $counts[$val]>1)
                {
                    if(!isset($this->second_header_keys['checkin_'.$val]))
                        $this->second_header_keys['checkin_'.$val] = $key;
                    else
                        $this->second_header_keys['checkout_'.$val] = $key;
                } else
                    $this->second_header_keys[$val] = $key;
            }
        }
    }
    protected function count_values($array)
    {
        if(!count($array))
            return [];
        $result = array();
        foreach($array as $value)
        {
            if(!is_int($value) && !is_string($value))
                continue;
            $value = strtolower(trim($value));
            if(isset($result[$value]))
                $result[$value]++;
            else
                $result[$value] = 1;
        }
        return $result;
    }

    protected function getBookings()
    {
        $bookings   = $this->getArrayByColName('booking id');
        //$bookings   = $this->allUpper($bookings);
        return $bookings;
    }
    protected function getHotels()
    {
        $hotels = $this->getUniqueColumn('hotel', true);
        $hotels = $this->allUpper($hotels);
        return $hotels;
    }
    protected function getAirlines()
    {
        $airlines = array_unique(array_merge(
            $this->getUniqueColumn('checkin_airline'), 
            $this->getUniqueColumn('checkout_airline')
        ));
        return $airlines;
    }
    protected function saveGetRoomTypes()
    {
        $room_types = $this->getColumnAsArray('room type', true);
        $rtref      = $this->saveGetUniqueRoomTypes($room_types);
        $room_types = $this->getIdByRoomType($room_types, $rtref);
        return $room_types;
    }
    protected function getIdByRoomType($room_types, $rtref)
    {
        foreach($room_types as $key=>$type)
        {
            $type_arr = explode(',', str_replace(' ', '', $type));
            foreach($type_arr as $k=>$v)
                $type_arr[$k] = $rtref[$v];
            $room_types[$key] = $type_arr;
        }
        return $room_types;
    }
    protected function saveGetUniqueRoomTypes($room_types)
    {
        $rts = explode(',', str_replace(' ', '', implode(',', $room_types)));
        $rts = $this->makeUnique($rts);
        $rts = $this->saveArrayToDB($this->makeArray2Dims($rts), 'App\RoomType', ['type']);
        return $rts;
    }
    protected function makeUnique($array)
    {
        if(empty($array))
            return [];
        $result=[];
        foreach($array as $value)
            $result[$value]=$value;
        return $result;
    }
    protected function getPassengers()
    {
        $bcindex= $this->second_header_keys['booking id'];
        $cindex = isset($this->second_header_keys['leader name'])?
                        $this->second_header_keys['leader name'] :
                        $this->second_header_keys['pax name'];
        $passengers = array();
        for($i=2; $i<count($this->result_array); $i++)
        {
            $value  = trim($this->result_array[$i][$cindex]);
            $key    = trim($this->result_array[$i][$bcindex]);
            $passengers[$key] = $this->passengerArrayByFullname($value);
        }
        return $passengers;
    }
    protected function getFlightNo($check='checkin')
    {
        $cindex     = $this->second_header_keys[$check.'_flight no.'];
        $flight_no  = array();
        for($i=2; $i<count($this->result_array); $i++)
        {
            $value = trim($this->result_array[$i][$cindex]);
            if(!empty($value))
                $flight_no[$i] = $value;
        }
        return $flight_no;
    }
    protected function getDateTimes($check='checkin')
    {
        $cindex     = $this->second_header_keys[$check.'_date & time'];
        $result     = array();
        $date_time  = array();
        for($i=2; $i<count($this->result_array); $i++)
        {
            $value = trim($this->result_array[$i][$cindex]);
            $result[] = $value;
        }
        for($i=0;$i<count($result);$i++)
        {
            $value = $result[$i];
            $date_time[$i]['date'] = $this->filterDate($value);
            $date_time[$i]['time'] = $this->filterTime($value);
            $arr = explode(' ', $value);
            for($j=0;$j<count($arr);$j++)
            {
                $field = $arr[$j];
                if(empty($field))
                    continue;
                $dort = $this->dateOrTime($field);
                $func = 'reValidate'.ucfirst($dort);
                $date_time[$i][$dort] = $this->$func($field);
            }
        }
        return $date_time;
    }
    protected function filterDate($datetime)
    {
        return $this->filterDT($datetime, 'date');
    }
    protected function filterTime($datetime)
    {
        return $this->filterDT($datetime, 'time');
    }
    protected function filterDT($datetime, $subject)
    {
        $datetime = trim($datetime);
        $result = null;
        $arr = explode(' ', $datetime);
        for($j=0;$j<count($arr);$j++)
        {
            $field = $arr[$j];
            if(empty($field))
                continue;
            $dort = $this->dateOrTime($field);
            if($dort != $subject)
                continue;
            $func = 'reValidate'.ucfirst($dort);
            $result = $this->$func($field);
        }
        return $result;
    }
    protected function dateOrTime($value)
    {
        if(count(explode(':', $value))>1)
            return 'time';
        return 'date';
        // $dt = new \DateTime($value);
        // if(explode(' ', $dt->format('Y-m-d H:i'))[1]=='00:00')
        //     return 'date';
        // return 'time';
    }
    protected function reValidateDate($date)
    {
        if(empty($date))
            return null;
        $dt = new \DateTime();
        $dt->setTimestamp(strtotime($date));
        if($dt->format('Y-m-d')=='1970-01-01'){
            $dt->setTimestamp(
                strtotime(
                    implode('/', array_reverse(
                        explode('/', $date)
                    ))
                )
            );
        }
        return $dt->format('Y-m-d');
    }
    protected function reValidateTime($time)
    {
        $dt = new \DateTime($time);
        return $dt->format('H:i');
    }
    protected function allUpper($array)
    {
        if(empty($array))
            return [];
        foreach($array as $key=>$value)
        {
            $array[$key] = strtoupper($value);
        }
        return $array;
    }
    protected function allLower($array)
    {
        if(empty($array))
            return [];
        foreach($array as $key=>$value)
        {
            $array[$key] = strtolower($value);
        }
        return $array;
    }
    protected function capAllWords($array)
    {
        if(empty($array))
            return [];
        foreach($array as $key=>$value)
        {
            $words = explode(' ', $value);
            foreach($words as $k=>$word)
            {
                $words[$k] = ucfirst(strtolower($word));
            }

            $array[$key] = implode(' ', $words);
        }
        return $array;
    }
    protected function getUniqueColumn($col, $case_insensetive=false)
    {
        $cindex = $this->second_header_keys[$col];
        $result = array();
        for($i=2; $i<count($this->result_array); $i++)
        {
            $key = trim($this->result_array[$i][$cindex]);
            if($case_insensetive)
                $key = strtolower($this->result_array[$i][$cindex]);
            if(!empty($key))
                $result[$key] = 0;
        }
        return array_keys($result);
    }
    protected function getColumnAsArray($col, $case_insensetive=false, $array_sep='')
    {
        $bcindex= $this->second_header_keys['booking id'];
        $cindex = $this->second_header_keys[$col];
        $result = array();
        for($i=2; $i<count($this->result_array); $i++)
        {
            $key    = trim($this->result_array[$i][$bcindex]);
            $value  = trim($this->result_array[$i][$cindex]);
            if($case_insensetive)
                $value=strtolower($value);
            if(!empty($value)){
                $result[$key] = $value;
                if(!empty($array_sep)) {
                    $result[$key] = explode($array_sep, $value);
                    foreach($result[$key] as $k=>$v)
                    {
                        if(!empty(trim($v)))
                            $result[$key][$k] = trim($v);
                        else
                            unset($result[$key][$k]);
                    }
                }
            }
        }
        return $result;
    }

    protected function getArrayByColName($col)
    {
        $bcindex= $this->second_header_keys['booking id'];
        $cindex = $this->second_header_keys[$col];
        $result = array();
        for($i=2; $i<count($this->result_array); $i++)
        {
            $key    = trim($this->result_array[$i][$bcindex]);
            $value  = trim($this->result_array[$i][$cindex]);
            if(!empty($value))
                $result[$key] = $value;
        }
        return $result;
    }

    protected function addFieldToArray($array, $field)
    {
        foreach($array as $key=>$value)
        {
            array_push($array[$key], $field);
        }
        return $array;
    }

    protected function makeArray2Dims($array, $sep='', $case_insensetive = false)
    {
        foreach($array as $key=>$value)
        {
            if($case_insensetive)
                $value=strtolower($value);
            if(!empty($sep))
                $array[$key] = explode($sep, $value);
            else
                $array[$key] = [$value];
            foreach($array[$key] as $k=>$v)
            {
                if(!empty(trim($v)))
                    $array[$key][$k] = trim($v);
                else
                    unset($array[$key][$k]);
                if(empty($array[$key]))
                    unset($array[$key]);
            }
        }
        return $array;
    }

    protected function saveArrayToDB($array, $model, $keys)
    {
        $result = [];
        foreach($array as $key=>$value)
        {
            $marr = [];
            foreach($value as $k=>$v){
                $prop     = $keys[$k];
                $marr[$prop] = $value[$k];
            }
            $unique_key = $this->unique_key($model, $marr);
            $result[$key] = isset($this->saved_cache[$unique_key])?
                                $this->saved_cache[$unique_key]:
                                $model::firstOrCreate($marr)->id;
            $this->saved_cache[$unique_key] = $result[$key];
        }
        return $result;
    }

    protected function saveGetPassengers()
    {
        $passengers = $this->getPassengers();
        $i = 0;
        foreach($passengers as $key=>$passenger)
        {
            $passengers[$key] = Passenger::create($passenger)->id;
        }
        return $passengers;
    }

    protected function saveGetHotels()
    {
        $city = City::where(['id'=>$this->city_id])->firstOrFail();
        return $this->saveArrayToDB(
                    $this->addFieldToArray(
                                $this->makeArray2Dims($this->getArrayByColName('hotel')),
                                $city->id),
                    'App\Hotel', ['title', 'city_id']);
    }

    protected function saveGetAirlines($check)
    {
        return $this->saveArrayToDB(
                    $this->makeArray2Dims($this->getArrayByColName($check.'_airline')),
                    'App\Airline', ['title']);
    }

    protected function passengerArrayByFullname($fullname)
    {
        $fields = explode(' ', $fullname);
        if(count($fields)==3)
            $passenger['title'] = array_shift($fields);
        $passenger['firstname'] = array_shift($fields);
        $passenger['lastname']  = array_shift($fields);
        return $passenger;
    }

    protected function unique_key($model, $array)
    {
        return $model.implode('',array_keys($array)).implode('',array_values($array));
    }

    protected function foreignId($obj_array, $model)
    {
        $uk = $this->unique_key($model, $obj_array);
        $model = 'App\\'.$model;
        $new_id = isset($this->saved_cache[$uk]) ?
                    $this->saved_cache[$uk] : $model::firstOrCreate($obj_array)->id;
        return $this->saved_cache[$uk] = $new_id;
    }
}
