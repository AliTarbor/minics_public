<?php

namespace App;

use App\ListLoader;

class TransferListLoader extends ListLoader
{
    protected $required_columns = [
        'booking id' => 1, 'booking date' => 1, 'hotel name' => 1, 'pax name' => 1,
        'no of pax' => 1, 'leaving from' => 1, 'date & time' => 2, 'supplier' => 2,
        'terminal' => 2, 'car name' => 2, 'car transfer type' => 2, 'airline' => 2,
        'flight no.' => 2, 'going to' => 1
    ];

    protected function getIDX()
    {
        $idx                    = array();
        $idx['booking_id']      = $this->second_header_keys['booking id'];
        $idx['booking_date']    = $this->second_header_keys['booking date'];
        $idx['hotel.title']     = $this->second_header_keys['hotel name'];
        $idx['passenger.name']  = $this->second_header_keys['pax name'];
        $idx['no_of_passengers']= $this->second_header_keys['no of pax'];

        $idx['checkin.city.title']      = $this->second_header_keys['leaving from'];
        $idx['checkin.supplier.title']  = $this->second_header_keys['checkin_supplier'];
        $idx['checkin.date & time']     = $this->second_header_keys['checkin_date & time'];
        $idx['checkin.terminal.title']  = $this->second_header_keys['checkin_terminal'];
        $idx['checkin.cartype.type']    = $this->second_header_keys['checkin_car name'];
        $idx['checkin.cartype.car_transfer_type.type']   = $this->second_header_keys['checkin_car transfer type'];
        $idx['checkin.airline.title']   = $this->second_header_keys['checkin_airline'];
        $idx['checkin.flight_no']       = $this->second_header_keys['checkin_flight no.'];

        $idx['checkout.city.title']     = $this->second_header_keys['going to'];
        $idx['checkout.supplier.title'] = $this->second_header_keys['checkout_supplier'];
        $idx['checkout.date & time']    = $this->second_header_keys['checkout_date & time'];
        $idx['checkout.terminal.title'] = $this->second_header_keys['checkout_terminal'];
        $idx['checkout.cartype.type']   = $this->second_header_keys['checkout_car name'];
        $idx['checkout.cartype.car_transfer_type.type']   = $this->second_header_keys['checkout_car transfer type'];
        $idx['checkout.airline.title']  = $this->second_header_keys['checkout_airline'];
        $idx['checkout.flight_no']      = $this->second_header_keys['checkout_flight no.'];
        return $idx;
    }

    protected function getUpdateCheckInfo($checkinout, $res, $idx, $last_checkid = 0)
    {
        $check      = new CheckInformation();
        if(!empty($last_checkid))
            $check = CheckInformation::where(['id' => $last_checkid])->first();
        $check->date  = $this->filterDate(trim($res[$idx[$checkinout.'.date & time']]));
        $check->time  = $this->filterTime(trim($res[$idx[$checkinout.'.date & time']]));
        
        $fno              = $res[$idx[$checkinout.'.flight_no']];
        $check->flight_no = !empty($fno) ? $fno : null;
        
        foreach(['airline', 'supplier', 'city'] as $value)
        {
            $val              = trim($res[$idx[$checkinout . '.' . $value . '.title']]);
            $identity         = $value . '_id';
            $check->$identity = !empty($val) ? 
                                    $this->foreignId(
                                        ['title' => $val],
                                        ucfirst($value)
                                    ) : null;
        }
        $value              = 'terminal';
        $val                = trim($res[$idx[$checkinout . '.' . $value . '.title']]);
        $identity           = $value . '_id';
        $check->$identity   = !empty($val) ?
                                $this->foreignId(
                                    ['title' => $val, 'city_id' => $this->city_id],
                                    ucfirst($value)
                                ) : null;
    
        $value      = 'car_transfer_type';
        $val        = trim($res[$idx[$checkinout . '.cartype.' . $value . '.type']]);
        $identity   = $value.'_id';
        $model      = str_replace(' ', '', ucwords(str_replace('_', ' ', $value)));
        $cttt       = !empty($val) ? $this->foreignId(['type' => $val], $model) : null;

        $type               = trim($res[$idx[$checkinout.'.cartype.type']]);
        $car_type_arr       = [$identity => $cttt, 'type' => $type];
        $check->car_type_id = !empty($type) ? $this->foreignId($car_type_arr, 'CarType') : null;

        $check->save();

        return $check;
    }

    public function storeToDB()
    {
        $idx    = $this->getIDX();

        for($i=2; $i<count($this->result_array); $i++)
        {
            $res    = $this->result_array[$i];

            \DB::transaction(function() use ($i, $res, $idx){
                $booking_id = trim($res[$idx['booking_id']]);
                $booking    = Booking::where(['booking_id' => $booking_id])->first();
                $passenger  = $this->passengerArrayByFullname($res[$idx['passenger.name']]);
                if(empty($booking->id)){
                    $booking                = new Booking();
                    $booking->booking_id    = $booking_id;
                    $passenger_id           = Passenger::create($passenger)->id;
                    $booking->passenger_id  = $passenger_id;
                    $booking->save();
                } else {
                    $booking->passenger()->update($passenger);
                }

                $booking->booking_date      = $this->reValidateDate(trim($res[$idx['booking_date']]));

                $booking->no_of_passengers  = !empty($res[$idx['no_of_passengers']])?intval($res[$idx['no_of_passengers']]):null;

                $val                = trim($res[$idx['hotel.title']]);
                $booking->hotel_id  = !empty($val)?
                                        $this->foreignId(['title'=>$val, 'city_id'=>$this->city_id],
                                        'Hotel'):null;

                foreach (['checkin', 'checkout'] as $checkinout) {
                    $check_id   = $checkinout.'_information_id';
                    $check      = $this->getUpdateCheckInfo($checkinout, $res, $idx,
                                                                empty($booking->$check_id) ?
                                                                null :
                                                                $booking->$check_id);                    
                    
                    $booking->$check_id = $check->id;
                    $booking->save();
                }

                }, 3);
        }
    }

}
