<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    use ManyHandler;

    protected $fillable = [
        'city_id', 'title', 'lc_user_id'
    ];

    public function city()
    {
        return $this->belongsTo(City::class,'city_id');
    }

    public function lcuser()
    {
        return $this->belongsTo(User::class,'lc_user_id');
    }

    public function booking()
    {
        return $this->hasMany(Booking::class);
    }
}
