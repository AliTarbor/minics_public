<?php

namespace App\Http\Controllers\city_admin;

use App\User;
use App\City;
use App\Hotel;
use App\UserRole;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    private $validate = [
        'username'  => 'required|unique:users',
        'password'  => 'required',
        'city_id'   => 'required',
        'role'      => 'required',
        'firstname' => 'nullable',
        'lastname'  => 'nullable',
        'phonenumber' => 'nullable'

    ];
    private $route = [
        'index' => 'user.index',
        'edit' => 'user.edit'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $func = auth()->user()->role.'Index';
        return $this->$func();
    }

    private function adminIndex()
    {
        $users      = User::latest()->paginate(20);
        $cities     = City::all();
        return view('dynamic-master', compact('users', 'cities'))->with(['page' => $this->route['index']]);
    }

    private function city_adminIndex()
    {
        $city_id = auth()->user()->city_id;
        $users  = $city_id ? User::where('role', '!=', UserRole::ADMIN)
                            ->where('role', '!=', UserRole::CITY_ADMIN)
                            ->where('city_id', '=', auth()->user()->city_id)
                            ->latest()->paginate(20) : [];
        return view('dynamic-master', compact('users'))->with(['page' => $this->route['index']]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $func = auth()->user()->role.'Store';
        return $this->$func($request);
    }

    private function adminStore($request)
    {
        $data = $request->validate($this->validate);
        $data['password'] = bcrypt($data['password']);
        $user = User::create($data);
        if(!empty($data['phonenumber']))
            $user->phonenumber()->create(['phonenumber'=>$data['phonenumber']]);
        return redirect()->route($this->route['index']);
    }

    private function city_adminStore($request)
    {
        unset($this->validate['city_id']);
        $data = $request->validate($this->validate);
        $city_id = auth()->user()->city_id;
        if(!$city_id)
            return redirect()->route('dashboard');
        if(!in_array($data['role'], [UserRole::REP, UserRole::LC, UserRole::DRIVER]))
            return redirect()->route($this->route['index']);
        $data['password'] = bcrypt($data['password']);
        $data['city_id']  = auth()->user()->city_id;
        $user = User::create($data);
        if(!empty($data['phonenumber']))
            $user->phonenumber()->create(['phonenumber'=>$data['phonenumber']]);
        return redirect()->route($this->route['index']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $func = auth()->user()->role.'Edit';
        return $this->$func($user);
    }

    private function adminEdit($user)
    {
        $cities      = City::all();
        $phonenumber = $user->phonenumber()->first();
        return view('dynamic-master', compact('user', 'cities', 'phonenumber'))->with(['page' => $this->route['edit']]);
    }

    private function city_adminEdit($user)
    {
        $city_id = auth()->user()->city_id;
        if(!$city_id)
            return redirect()->route('dashboard');
        $phonenumber = $user->phonenumber()->first();
        return view('dynamic-master', compact('user', 'phonenumber'))->with(['page' => $this->route['edit']]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $func = auth()->user()->role.'Update';
        return $this->$func($request, $user);
    }

    private function adminUpdate($request, $user)
    {
        $this->validate['password'] = 'nullable';
        $this->validate['username'] = $this->validate['username'] . ',username,' . $user->id;
        $data = $request->validate($this->validate);
        if(!empty($data['password']))
            $data['password'] = bcrypt($data['password']);
        else
            unset($data['password']);
            if($user->role == UserRole::LC){
                if($data['role']!=UserRole::LC || $data['city_id'] != $user->city_id)
                    Hotel::where(['lc_user_id' => $user->id])->update(['lc_user_id' => null]);
            }
            $user->update($data);
        $user->phonenumber()->delete();
        if(!empty($data['phonenumber']))
            $user->phonenumber()->create(['phonenumber'=>$data['phonenumber']]);
        return redirect()->route($this->route['index']);
    }

    private function city_adminUpdate($request, $user)
    {
        unset($this->validate['city_id']);
        $this->validate['password'] = 'nullable';
        $this->validate['username'] = $this->validate['username'] . ',username,' . $user->id;
        $data = $request->validate($this->validate);
        $city_id = auth()->user()->city_id;
        if(!$city_id || $user->city_id!=$city_id)
            return redirect()->route('dashboard');
        if(!empty($data['password']))
            $data['password'] = bcrypt($data['password']);
        else
            unset($data['password']);
        if($user->role == UserRole::LC){
            if($data['role']!=UserRole::LC)
                Hotel::where(['lc_user_id' => $user->id])->update(['lc_user_id' => null]);
        }
        $user->update($data);
        $user->phonenumber()->delete();
        if(!empty($data['phonenumber']))
            $user->phonenumber()->create(['phonenumber'=>$data['phonenumber']]);
        return redirect()->route($this->route['index']);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $func = auth()->user()->role.'Destroy';
        return $this->$func($user);
    }

    private function adminDestroy($user)
    {
        $user->delete();
        return redirect()->route($this->route['index']);
    }

    private function city_adminDestroy($user)
    {
        $city_id = auth()->user()->city_id;
        if(!$city_id || $user->city_id!=$city_id)
            return redirect()->route('dashboard');
        $user->delete();
        return redirect()->route($this->route['index']);
    }

}
