<?php

namespace App\Http\Controllers\Admin;

use App\City;
use App\Timezone;
use App\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CitiesController extends Controller
{
    private $validate = [
        'timezone_id' => 'required',
        'country_id' => 'required',
        'title' => 'required|max:50'
    ];
    private $route = [
        'index' => 'city.index',
        'edit' => 'city.edit'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities     = City::latest()->paginate(20);
        $countries  = Country::all();
        $timezones  = Timezone::all();
        return view('dynamic-master', compact('cities', 'countries', 'timezones'))->with(['page' => $this->route['index']]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $city = $request->validate($this->validate);
        City::create($city);
        return redirect()->route($this->route['index']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function show(City $city)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city)
    {
        $timezones = Timezone::all();
        $countries = Country::all();
        return view('dynamic-master', compact('city', 'countries', 'timezones'))->with(['page' => $this->route['edit']]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, City $city)
    {
        $data = $request->validate($this->validate);
        $city->update($data);
        return redirect()->route($this->route['index']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $city)
    {
        $city->delete();
        return redirect()->route($this->route['index']);
    }
}
