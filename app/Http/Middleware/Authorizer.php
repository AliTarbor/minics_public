<?php

namespace App\Http\Middleware;

use Closure;

class Authorizer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if(in_array(auth()->user()->role, explode('+', $role)))
            return $next($request);
        return redirect(route('login'));
    }
}
