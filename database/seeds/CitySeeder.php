<?php

use Illuminate\Database\Seeder;
use App\City;
use App\Country;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        City::create(['title'  => 'Tehran', 'country_id' => Country::where(['title'=>'Iran'])->first()->id]);
        City::create(['title'  => 'Dubai', 'country_id' => Country::where(['title'=>'United Arab Emirates'])->first()->id]);
    }
}
