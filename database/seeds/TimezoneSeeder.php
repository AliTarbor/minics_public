<?php

use Illuminate\Database\Seeder;
use App\Timezone;

class TimezoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $JSON_timezones = Timezone::allJSON();

        foreach ($JSON_timezones as $timezone) {
            Timezone::create([
                'name'      => ((isset($timezone['name'])) ? $timezone['name'] : null),
                'abbr'      => ((isset($timezone['abbr'])) ? $timezone['abbr'] : null),
                'offset'    => ((isset($timezone['offset'])) ? $timezone['offset'] : null),
                'isdst'     => ((isset($timezone['isdst'])) ? $timezone['isdst'] : null),
                'text'      => ((isset($timezone['text'])) ? $timezone['text'] : null),
            ]);
        }
    }
}
