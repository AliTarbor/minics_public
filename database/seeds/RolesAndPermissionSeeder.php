<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()['cache']->forget('spatie.permission.cache');
 
        $role_permissions = [
            'admin' => [
                'manage user', 'manage user.city_id', 'manage user.role', 'manage country',
                'manage city', 'manage timezone', 'manage hotel.city_id'    
            ],
            'city_admin' => [
                'manage passenger', 'manage passenger.phonenumber',
                'manage passenger_type', 'manage hotel', 'manage booking', 'see booking',
                'manage booking.check_by_rep', 'manage booking.check_by_lc',
                'manage booking.passenger.type_id', 'manage booking.no_show', 'manage booking.room_number'
            ],
            'lc' => [
                'manage passenger.phonenumber', 'see booking', 'manage booking.check_by_lc',
                'manage booking.passenger.type_id', 'manage booking.room_number'
            ],
            'rep' => [
                'manage passenger.phonenumber', 'see booking', 'manage booking.check_by_rep',
                'manage booking.passenger.type_id', 'manage booking.room_number'
            ],
            'driver' => [
                'see booking', 'manage booking.no_show'
            ]
        ];

        $perms = array_unique(array_merge($role_permissions['admin'], $role_permissions['city_admin'],
            $role_permissions['lc'], $role_permissions['rep'], $role_permissions['driver']));
        foreach($perms as $permit)
        {
            Permission::create(['name' => $permit]);
        }
        $role = Role::create(['name' => 'city_admin']);
        $role->givePermissionTo($role_permissions['city_admin']);

        $role = Role::create(['name' => 'lc']);
        $role->givePermissionTo($role_permissions['lc']);

        $role = Role::create(['name' => 'rep']);
        $role->givePermissionTo($role_permissions['rep']);

        $role = Role::create(['name' => 'driver']);
        $role->givePermissionTo($role_permissions['driver']);

        $role = Role::create(['name' => 'admin']);
        $role->givePermissionTo(Permission::all());
    }
}
